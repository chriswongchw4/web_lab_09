

function clock() {
    var clock = new Date();
    document.getElementById("clock").innerHTML = clock.toLocaleTimeString();

}

var ID = null;
ID = setInterval(clock, 1000);

function resumeClock(){
        ID = setInterval(clock, 1000);
}


function stopClock(){
    if (ID!=null){
        clearInterval(ID);
    }
}